starman (0.4017-1) unstable; urgency=medium

  * Team upload
  * Import upstream version 0.4017.
  * Declare compliance with Debian Policy 4.6.2.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Thu, 05 Oct 2023 23:21:44 +0200

starman (0.4016-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libnet-server-perl,
      libtest-tcp-perl.

  [ Yadd ]
  * Bump debhelper from old 12 to 13
  * Import upstream version 0.4016
  * Declare compliance with policy 4.6.1

 -- Yadd <yadd@debian.org>  Fri, 16 Sep 2022 06:35:40 +0200

starman (0.4015-1) unstable; urgency=medium

  * Import upstream version 0.4015
  * Remove patch now included in upstream
  * Add "Rules-Requires-Root: no"

 -- Xavier Guimard <yadd@debian.org>  Sun, 27 Oct 2019 20:39:19 +0100

starman (0.4014-3) unstable; urgency=medium

  * Team upload

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ Damyan Ivanov ]
  * replace test certificates with 2048-bit ones (Closes: #912247)
  * declare conformance with Policy 4.2.1 (no changes needed)
  * bump debhelper compatibility level to 11

 -- Damyan Ivanov <dmn@debian.org>  Mon, 29 Oct 2018 20:57:51 +0000

starman (0.4014-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * declare conformance with Policy 4.1.4 (no changes needed)
  * Remove unnecessary dependency version for libplack
  * Bump debhelper compatibility level to 10
  * Add myself in uploaders
  * Add libnet-server-ss-prefork-perl in dependencies (Closes: #893090)

 -- Xavier Guimard <x.guimard@free.fr>  Wed, 11 Apr 2018 13:48:49 +0200

starman (0.4014-1) unstable; urgency=medium

  * Team upload
  * Import upstream version 0.4014
    Fixes "FTBFS with Plack 1.0036"
    (Closes: #789838)

 -- gregor herrmann <gregoa@debian.org>  Thu, 25 Jun 2015 21:21:53 +0200

starman (0.4011-1) unstable; urgency=medium

  * Team upload
  * Import upstream version 0.4011

 -- gregor herrmann <gregoa@debian.org>  Sun, 10 May 2015 17:11:48 +0200

starman (0.4010-1) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Imported upstream version 0.4010
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Fri, 10 Oct 2014 14:04:34 +0200

starman (0.4009-1) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * debian/control: bump required debhelper version for
    Module::Build::Tiny to 9.20140227.
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Import Upstream version 0.4009
  * Update upstream copyright years
  * Declare compliance with Debian Policy 3.9.5

 -- Florian Schlichting <fsfs@debian.org>  Fri, 11 Apr 2014 21:14:58 +0200

starman (0.4008-1) unstable; urgency=low

  * New upstream release
  * add myself to uploaders
  * libnet-server-perl versioned depends
  * libmodule-build-tiny-perl versioned depends

 -- CSILLAG Tamas <cstamas@cstamas.hu>  Tue, 15 Oct 2013 22:17:43 +0200

starman (0.3014-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Update build dependencies.
  * Don't install empty manpages.

 -- gregor herrmann <gregoa@debian.org>  Wed, 24 Jul 2013 19:58:42 +0200

starman (0.3007-1) unstable; urgency=low

  * New upstream release
  * Remove inc/* sections for copyright
  * Update upstream copyright according to new LICENSE file

 -- Alessandro Ghedini <ghedo@debian.org>  Fri, 29 Mar 2013 12:54:25 +0100

starman (0.3006-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Alessandro Ghedini ]
  * New upstream release

 -- Alessandro Ghedini <ghedo@debian.org>  Tue, 12 Feb 2013 14:17:50 +0100

starman (0.3005-1) unstable; urgency=low

  * New upstream release
  * Recommend libserver-starter-perl

 -- Alessandro Ghedini <ghedo@debian.org>  Sun, 18 Nov 2012 19:57:57 +0100

starman (0.3003-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Alessandro Ghedini ]
  * New upstream release
  * Bump Standards-Version to 3.9.4 (no changes needed)

 -- Alessandro Ghedini <ghedo@debian.org>  Mon, 08 Oct 2012 10:08:06 +0200

starman (0.3001-1) unstable; urgency=low

  * New upstream release
  * Email change: Alessandro Ghedini -> ghedo@debian.org
  * Update copyright to Copyright-Format 1.0
  * Bump Standards-Version to 3.9.3 (no changes needed)
  * Update copyright years for Module::Install

 -- Alessandro Ghedini <ghedo@debian.org>  Tue, 26 Jun 2012 17:53:11 +0200

starman (0.3000-1) unstable; urgency=low

  * New upstream release
  * Explicit (build) depends on libhttp-date-perl
  * Update upstream copyright years

 -- Alessandro Ghedini <al3xbio@gmail.com>  Wed, 22 Feb 2012 18:36:22 +0100

starman (0.2014-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Alessandro Ghedini ]
  * New upstream release

 -- Alessandro Ghedini <al3xbio@gmail.com>  Mon, 19 Sep 2011 11:40:30 +0200

starman (0.2013-1) unstable; urgency=low

  * New upstream release (Closes: #633764)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Thu, 14 Jul 2011 21:42:11 +0200

starman (0.2012-1) unstable; urgency=low

  * New upstream release

 -- Alessandro Ghedini <al3xbio@gmail.com>  Thu, 23 Jun 2011 16:13:25 +0200

starman (0.2011-1) unstable; urgency=low

  * New upstream release
  * Update copyright for inc/Module/*
  * Bump Standards-Version to 3.9.2 (no changes needed)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Wed, 25 May 2011 20:24:15 +0200

starman (0.2010-1) unstable; urgency=low

  * New upstream release
  * Bump debhelper compat level to 8
  * Add libtest-requires-perl and libtest-tcp-perl to B-D-I
  * Versioned B-D-I on libplack-perl

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sun, 03 Apr 2011 18:56:15 +0200

starman (0.2008-1) unstable; urgency=low

  * Initial Release. (Closes: #610904)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sat, 19 Feb 2011 14:25:48 +0100
